import os

from flask import Flask, url_for

from project.website.routes import bp as website_bp

app = Flask(__name__)
if os.environ['FLASK_ENV'] == 'production':
	app.config['SECRET_KEY'] = os.environ['SECRET_KEY']
	app.config['TESTING'] = False
elif os.environ['FLASK_ENV'] == 'development':
	app.config.from_pyfile('settings.cfg')

app.jinja_env.add_extension('jinja2.ext.do')
app.jinja_env.auto_reload = True

app.register_blueprint(website_bp)

@app.context_processor
def override_url_for():
	return dict(url_for=dated_url_for)

@app.context_processor
def insert_testing():
	return dict(TESTING=app.config['TESTING'])

def dated_url_for(endpoint, **values):
	if endpoint == 'static':
		filename = values.get('filename', None)
		if filename:
			file_path = os.path.join(app.root_path,
									 endpoint,
									 filename)

			values['q'] = int(os.stat(file_path).st_mtime)

	return url_for(endpoint, **values)
