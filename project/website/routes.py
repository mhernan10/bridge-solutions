from flask import Blueprint, render_template, current_app as app

bp = Blueprint('website', __name__, template_folder='templates/website')

@bp.route('/')
def index():
	return render_template('index.html')


@bp.route('/<page_id>')
def pages(page_id='index'):
	return render_template('{page_id}.html'.format(page_id=page_id))
